<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class TestController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
    }
################################################################################
    public function basic()
    {
        $view = new ViewModel();
        //Route
        $view->lang = $this->params()->fromRoute('lang', 'th');
        $view->action = $this->params()->fromRoute('action', 'index');
        $view->id = $this->params()->fromRoute('id', '');
        $view->page = $this->params()->fromQuery('page', 1);
        return $view;       
    } 
################################################################################
    public function indexAction() 
    {
        try
        {
            $view = $this->basic();
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
################################################################################
    public function findingAction()
    {
        try{
            $view = $this->basic();
            $x = $this->params()->fromPost('x', 0);
            
            $i = 1 + $x;
            $j = 0 + $x;

            $result = 3+($i*$j);
            
            $data = array(
                            'status' => 200,
                            'items' => $result
                        );
                        
            echo json_encode($data);
            exit; 
            
            return $view;
        } catch( Exception $e ) {
            print_r($e);
        }
    }
    function mapAction()
    {
        $view = $this->basic();
        $key = 'AIzaSyBdJTzO14hVGJiyPkWMXGKIrCflAAcq8Fw';
        $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants+in+Bang+Sue,+Bangkok&radius=10000&key=".$key;
    
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$arr = json_decode($data, true);
		
		
		$item = $arr['results'];
		//print_r($item);exit();
		
		curl_close($ch);
        
        $result = array(
                            'status' => 200,
                            'items' => $item
                        );
        echo json_encode($result, true);
        exit;
        return $view;
    }
################################################################################
/*
    Memcache
*/
################################################################################
	function MemCache()
    {
    	$cache = StorageFactory::factory([
            'adapter' => [
                'name'    => 'apc',
                'options' => ['ttl' => 3600],
            ],
            'plugins' => [
                'exception_handler' => ['throw_exceptions' => false],
            ],
        ]);
        
        // Alternately, create the adapter and plugin separately:
        $cache  = StorageFactory::adapterFactory('apc', ['ttl' => 3600]);
        $plugin = StorageFactory::pluginFactory('exception_handler', [
            'throw_exceptions' => false,
        ]);
        $cache->addPlugin($plugin);
        
        // Or do it completely manually:
        $cache  = new Zend\Cache\Storage\Adapter\Apc();
        $cache->getOptions()->setTtl(3600);
        
        $plugin = new Zend\Cache\Storage\Plugin\ExceptionHandler();
        $plugin->getOptions()->setThrowExceptions(false);
        $cache->addPlugin($plugin);
        
		return($cache);
	}
}